<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/javascripts/vendor/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
	<script src="//use.typekit.net/llz8mac.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
</head>
<body <?php body_class(); ?>>
	<?php if ( is_home() ) : ?>
	<?php else : ?>
		<header class="header header-article" role="banner">
			<a class="button-back" href="<?php echo esc_url( home_url( '/' ) ); ?>">Zurück zur Homepage</a>
		</header>
	<?php endif; ?>
