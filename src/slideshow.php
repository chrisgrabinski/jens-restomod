<div class="playground-canvas canvas-slideshow">
  <button class="slide-nav slide-nav-prev">Prev</button>
  <button class="slide-nav slide-nav-next">Next</button>
  <button class="slide-nav slide-nav-close btn-close">Close</button>
  <div class="slides">
    <?php

    $args = array(
        'post_type' => 'part',
        'posts_per_page' => 50,
        'orderby' => 'ASC'
    );

    $the_query = new WP_Query( $args );?>

    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <div class="slide" data-part="<?php echo get_the_slug(); ?>">
        <div class="slide-content">
          <?php if ( has_post_thumbnail() ) { ?>
            <?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'slide-image' ) ); ?>
          <?php } ?>
          <div class="slide-meta">
            <a class="slide-url" href="<?php echo types_render_field("tag-url", array("output" => "raw")) ?>">
              <h2 class="slide-name"><?php the_title(); ?></h2>
              <p class="slide-description"><?php echo get_the_content(); ?></p>
            </a>
          </div>
        </div>
      </div>
    <?php endwhile; else : ?>
      <p>There were no parts. </p>
    <?php endif; wp_reset_postdata(); ?>
  </div>
</div>
