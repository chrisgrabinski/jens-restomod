<?php
/**
* The template for displaying all single posts and attachments
*
* @package WordPress
* @subpackage Twenty_Fifteen
* @since Twenty Fifteen 1.0
*/

get_header(); ?>

	<main class="content">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>

		<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/BlogPosting">

		<div class="article-container">
			<header class="article-header">
				<?php if ( has_post_thumbnail() ) { ?>
					<meta itemprop="image" content="<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); echo $large_image_url[0]; ?>">
				<?php } ?>
				<meta itemprop="url" content="<?php the_permalink(); ?>">
				<h1 class="article-name scrollme animateme" data-when="exit" data-from="0" data-to="1" data-translatey="-100" data-easing="easeout" itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></h1></a>
				<p class="article-description scrollme animateme" data-when="exit" data-from="0" data-to="1" data-translatey="-100" data-easing="easeout" itemprop="description"><?php $my_excerpt = get_the_excerpt(); echo $my_excerpt; ?></p>
				<time class="article-date scrollme animateme" data-when="exit" data-from="0" data-to="0.5" data-opacity="0" data-easing="linear" datetime="<?php the_time('c'); ?>" itemprop="datePublished" content="<?php the_time('c'); ?>"><?php the_time('j. F Y'); ?></time>
			</header>
			<div class="article-body" itemprop="articleBody">
				<?php the_content(); ?>
			</div>
			<footer class="article-footer scrollme">
				<div class="article-social">
					<ul>
						<li>
							<a class="socialLink" title="Teile diesen Artikel auf Facebook" rel="nofollow" onclick="window.open('//www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo str_replace(' ', '%20', get_the_title()) ?>&amp;p[summary]=<?php $my_excerpt = get_the_excerpt(); echo str_replace(' ', '%20', $my_excerpt); ?>&amp;p[url]=<?php the_permalink(); ?><?php if ( has_post_thumbnail() ): ?>&amp;p[images][0]=<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); echo $large_image_url[0]; endif; ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)">
							<svg class="socialIcon socialIcon-facebook" x="0px" y="0px" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
							<g class="socialIcon-logo">
							<path d="M44,42.5c0,0.8-0.7,1.5-1.5,1.5h-6v-9.3h3l0.5-3.5h-3.5v-2.7c0-1,0.5-1.5,1.5-1.5h2.2v-3.1c0,0-1-0.2-2.5-0.2
							c-3.3,0-5,1.8-5,4.6v3h-3v3.5h3V44H21.5c-0.8,0-1.5-0.7-1.5-1.5v-21c0-0.8,0.7-1.5,1.5-1.5h21c0.8,0,1.5,0.7,1.5,1.5V42.5z"/>
							</g>
							<circle class="socialIcon-circle-01" fill="none" stroke-miterlimit="10" cx="32" cy="32" r="30"/>
							<circle class="socialIcon-circle-02" fill="none" stroke-miterlimit="10" cx="32" cy="32" r="30"/>
							</svg>
							</a>
							</li>
							<li>
							<a class="socialLink" title="Teile diesen Artikel auf Twitter" rel="nofollow" onclick="window.open('//twitter.com/intent/tweet?button_hashtag=restomod&text=<?php echo str_replace(' ', '%20', get_the_title()) ?>%20auf%20<?php bloginfo('name'); ?>%20<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=548,height=325');" data-related="jensamende" data-url="<?php the_permalink(); ?>" href="javascript: void(0)">
							<svg class="socialIcon socialIcon-twitter" x="0px" y="0px" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
							<g class="socialIcon-logo">
							<path d="M43.9,26c0,0.3,0,0.5,0,0.8c0,8-6.2,17.2-17.5,17.2c-3.5,0-6.7-0.8-9.4-2.5c0.5,0.1,1,0.1,1.5,0.1c2.9,0,5.6-1.2,7.6-2.9
							c-2.7,0-5-1.8-5.7-4.2c0.4,0.1,0.8,0.1,1.1,0.1c0.6,0,1.1-0.1,1.6-0.2c-2.8-0.5-4.9-3-4.9-5.9c0,0,0,0,0-0.1
							c0.8,0.4,1.8,0.7,2.8,0.8c-1.7-1.1-2.8-2.9-2.8-5c0-1.1,0.3-2,0.8-2.9c3.1,3.7,7.6,5.9,12.7,6.2c-0.1-0.4-0.1-0.9-0.1-1.4
							c0-3.3,2.7-6,6.1-6c1.8,0,3.4,0.8,4.5,1.9c1.4-0.3,2.7-0.8,3.9-1.5c-0.5,1.4-1.4,2.6-2.7,3.4c1.3-0.1,2.4-0.5,3.5-1
							C46.2,24.1,45.1,25.1,43.9,26z"/>
							</g>
							<circle class="socialIcon-circle-01" fill="none" stroke-miterlimit="10" cx="32" cy="32" r="30"/>
							<circle class="socialIcon-circle-02" fill="none" stroke-miterlimit="10" cx="32" cy="32" r="30"/>
							</svg>
							</a>
							</li>
							</ul>
							</div>
							<nav class="post-navigation">
								<div class="post-prev">
									<?php echo get_previous_post_link( '<h2 class="post-navigation-title animateme scrollme" data-when="enter" data-from="0.2" data-to="0.1" data-opacity="0" data-easing="easein">Vorheriger Beitrag</h2><div class="nav-previous animateme" data-when="exit" data-from="1" data-to="0.9" data-translatey="50" data-easing="easeout">%link</div>' ); ?>
								</div>
								<div class="post-next">
									<?php echo get_next_post_link( '<h2 class="post-navigation-title animateme scrollme" data-when="enter" data-from="0.2" data-to="0.1" data-opacity="0" data-easing="easein">Nächster Beitrag</h2><div class="nav-next animateme" data-when="exit" data-from="1" data-to="0.9" data-translatey="50" data-opacity="0" data-easing="easeout">%link</div>' ); ?>
								</div>
							</nav>
							</footer>
		</div>
	</article>
		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
