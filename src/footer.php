<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 */
?>


<footer class="footer">
	<img class="footer-logo" src="<?php echo get_template_directory_uri(); ?>/images/vbr-logo.svg" alt="Vintage Bike Restomod">
	<p>Ein Projekt von <a href="http://www.jensamende.de">Jens Amende</a></p>
	<ul>
		<li><a href="impressum.html">Impressum</a></li><li><a href="datenschutz.html">Datenschutzbestimmungen</a></li>
	</ul>
</footer>
<?php wp_footer(); ?>
</body>
</html>
