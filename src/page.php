<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<main class="content">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>

		<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Article">
			<?php $image = get_field('article_header_image'); if( !empty($image) ): ?>
			<style>
			#post-<?php the_ID(); ?>
			{
				background-image: url('<?php echo $image['url']; ?>');
			}
			</style>
		<?php endif; ?>

		<div class="article-container">
			<header class="article-header">
				<?php if ( has_post_thumbnail() ) {?>
					<meta itemprop="image" content="<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); echo $large_image_url[0]; ?>">
					<?php } ?>
					<meta itemprop="url" content="<?php the_permalink(); ?>">
					<h1 class="article-name" itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></h1></a>
					<meta itemprop="datePublished" content="<?php the_time('c'); ?>">
				</header>
				<div class="article-body" itemprop="articleBody">
					<?php the_content(); ?>
				</div>
			</div>
		</article>

		<?php endwhile; ?>

</main>

<?php get_footer(); ?>
