<?php

/*
* Let WordPress manage the document title.
* By adding theme support, we declare that this theme does not use a
* hard-coded <title> tag in the document head, and expect WordPress to
* provide it for us.
*/

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );

define("THEME_DIR", get_template_directory_uri());
remove_action('wp_head', 'wp_generator');

function enqueue_styles() {
  wp_register_style( 'screen-style', THEME_DIR . '/style.css', array(), '1', 'all' );
  wp_enqueue_style( 'screen-style' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles' );

function enqueue_scripts() {

  wp_register_script( 'modernizr', THEME_DIR . '/javascripts/vendor/modernizr.min.js', array(), '1', true );
  wp_enqueue_script( 'modernizr' );

  //wp_register_script( 'scrollme', THEME_DIR . '/javascripts/vendor/jquery.scrollme.min.js', array( 'jquery' ), '1', false );
  //wp_enqueue_script( 'scrollme' );

  wp_register_script( 'mainfile', THEME_DIR . '/javascripts/main.js', array( 'jquery' ));
  wp_enqueue_script( 'mainfile' );

}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

function custom_excerpt_length( $length ) {
  return 156;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function get_the_slug( $id=null ){
  if( empty($id) ):
    global $post;
    if( empty($post) )
      return ''; // No global $post var available.
    $id = $post->ID;
  endif;

  $slug = basename( get_permalink($id) );
  return $slug;
}

?>
