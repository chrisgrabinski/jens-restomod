jQuery(document).ready(function($) {

$('.button-wheels').on('click', function() {
  $('body').toggleClass('wheel-stop')
});

$('.button-gears').on('click', function() {
  $('body').toggleClass('gears-active')
});

$('.bike-tag').on('click', function() {
  var targetComponent = $(this).attr('data-part');
  $('.slide-current').removeClass('slide-current');
  console.log($('.slide-current'));

  $('.canvas-slideshow')
  .find('.slide[data-part=' + targetComponent + ']')
  .addClass('slide-current');

  if ( $('.slide-current').is(':last-child') ) {
    $('.slide-nav-next').addClass('is-inactive');
  } else if ( $('.slide-current').is(':first-child') ) {
    $('.slide-nav-prev').addClass('is-inactive');
  } else {
    $('.slide-nav').removeClass('is-inactive');
  }
})

$('.toggle').on('click', function() {
  $(this).toggleClass('toggle-is-active');
  $('.bike-container').toggleClass('bike-container-show-tags');
});

$('.bike-tag, .slide-nav-close').on('click', function() {
  $('.toggle').removeClass('toggle-is-active');
  $('.bike-container').removeClass('bike-container-show-tags');
  $('.playground').toggleClass('playground-is-bike').toggleClass('playground-is-slideshow');
});


$('.slide-nav-next, .slide-nav-prev').on('click', function() {

  var current = $('.slide-current');

  if ( $(this).hasClass('slide-nav-next') && current.is(':last-child') === false ) {
    current.removeClass('slide-current');
    current.next().addClass('slide-current');
  } else if ( $(this).hasClass('slide-nav-prev') && current.is(':first-child') === false ) {
    current.removeClass('slide-current');
    current.prev().addClass('slide-current');
  }

  if ( $('.slide-current').is(':last-child') ) {
    $('.slide-nav-next').addClass('is-inactive');
  } else if ( $('.slide-current').is(':first-child') ) {
    $('.slide-nav-prev').addClass('is-inactive');
  } else {
    $('.slide-nav').removeClass('is-inactive');
  }

});

});
