'use strict';

var gulp        = require('gulp'),
    $           = require('gulp-load-plugins')();

gulp.task('default', function() {
    gulp.start(['build']);
});

// Image Task
gulp.task('images', function () {
    gulp.src('src/images/*')
    .pipe(gulp.dest('wordpress/wp-content/themes/vintage-bike-restomod/images'))
    .pipe($.size());
});

// Styles Task
// Autoprefix and compile SASS
gulp.task('styles', function() {
    return gulp.src('src/stylesheets/style.scss')
    .pipe($.rubySass({
        style: 'expanded',
        precision: 10,
        'sourcemap=none': true
        // This solves an issue with autoprefixer and sourcemaps
        // https://github.com/sindresorhus/gulp-ruby-sass/issues/113#issuecomment-53778451
    }))
    .on('error', function (err) { console.log(err.message); })
    .pipe($.autoprefixer(['last 2 versions', '> 5%']))
    .pipe(gulp.dest('wordpress/wp-content/themes/vintage-bike-restomod'))
    .pipe($.size());
});

// Scripts Task
gulp.task('scripts', function() {
    gulp.src('src/javascripts/*.js')
    .pipe(gulp.dest('wordpress/wp-content/themes/vintage-bike-restomod/javascripts'))
    .pipe($.size());

    gulp.src('src/javascripts/vendor/*.js')
    .pipe(gulp.dest('wordpress/wp-content/themes/vintage-bike-restomod/javascripts/vendor'))
    .pipe($.size());
});

// PHP Tasks
gulp.task('php', function() {
    gulp.src('src/*.php')
    .pipe(gulp.dest('wordpress/wp-content/themes/vintage-bike-restomod'))
    .pipe($.size());
});

// Watch Task
gulp.task('watch', ['styles'], function () {
  gulp.watch(['src/**/*.php'], ['php']);
  gulp.watch(['src/stylesheets/**/*.{scss,css}'], ['styles']);
  gulp.watch(['src/javascripts/**/*.js'], ['scripts']);
  gulp.watch(['src/images/**/*'], ['images']);
});



// Build Task
gulp.task('build', ['styles', 'scripts', 'php', 'images']);
